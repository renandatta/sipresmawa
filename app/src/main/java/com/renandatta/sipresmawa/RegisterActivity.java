package com.renandatta.sipresmawa;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private Button buttonRegister,buttonBatal;
    private EditText editNoID,editName,editPhone,editEmail,editPassword;
    private Boolean checkSudahAda = false;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        buttonRegister = findViewById(R.id.buttonRegister);
        buttonBatal = findViewById(R.id.buttonBatal);
        editNoID = findViewById(R.id.editNoID);
        editName = findViewById(R.id.editName);
        editPhone = findViewById(R.id.editPhone);
        editEmail = findViewById(R.id.editEmail);
        editPassword = findViewById(R.id.editPassword);

        buttonRegister.setOnClickListener(this);
        buttonBatal.setOnClickListener(this);

        db = FirebaseFirestore.getInstance();
    }

    @Override
    public void onClick(View v){
        int idButton = v.getId();
        if(idButton == R.id.buttonRegister){
            registerUser();
        }
        if(idButton == R.id.buttonBatal){
            finish();
            Intent loginActivity = new Intent(this,LoginActivity.class);
            startActivity(loginActivity);
        }
    }

    public void registerUser(){
        final String no = editNoID.getText().toString();
        final String name = editName.getText().toString();
        final String phone = editPhone.getText().toString();
        final String email = editEmail.getText().toString();
        final String password = editPassword.getText().toString();

        if((no.equals("")) || (name.equals("")) || (phone.equals("")) || (email.equals("")) || (password.equals(""))){
            showMessage("Lengkapi data !");
        }else{
            String[] emailCheck = email.split("@");
            if(emailCheck.length < 2){
                showMessage("Email salah !");
            }else{
                db.collection("users")
                    .whereEqualTo("no_id", no)
                    .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (DocumentSnapshot document : task.getResult()) {
                                    checkSudahAda = true;
                                    showMessage("No.ID sudah ada !");
                                }
                                if(!checkSudahAda){
                                    Map<String, Object> user = new HashMap<>();
                                    user.put("no_id", no);
                                    user.put("name", name);
                                    user.put("phone", phone);
                                    user.put("email", email);
                                    user.put("password", password);
                                    db.collection("users")
                                            .add(user)
                                            .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                                @Override
                                                public void onSuccess(DocumentReference documentReference) {
                                                    showMessage("Register berhasil !");
                                                    SharedPreferences pref = getApplicationContext().getSharedPreferences("sipresmawa", 0);
                                                    SharedPreferences.Editor editor = pref.edit();
                                                    editor.putString("no_id", no);
                                                    editor.apply();

                                                    finish();
                                                    Intent mainActivity = new Intent(RegisterActivity.this,MainActivity.class);
                                                    startActivity(mainActivity);
                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    showMessage("Register gagal !");
                                                }
                                            });
                                }
                            }
                        }
                    });
            }
        }
    }

    public void showMessage(String text){
        AlertDialog.Builder alert  = new AlertDialog.Builder(this);
        alert.setMessage(text);
        alert.setPositiveButton("OK", null);
        alert.create().show();
    }
}
