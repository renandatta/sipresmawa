package com.renandatta.sipresmawa;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button buttonLogin,buttonRegister;
    private EditText editEmail,editPassword;
    private Boolean checkAda = false;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        buttonLogin = findViewById(R.id.buttonLogin);
        buttonRegister = findViewById(R.id.buttonRegisterLogin);
        editEmail = findViewById(R.id.editEmail);
        editPassword = findViewById(R.id.editPassword);

        buttonLogin.setOnClickListener(this);
        buttonRegister.setOnClickListener(this);

        db = FirebaseFirestore.getInstance();
    }

    @Override
    public void onClick(View v){
        int idButton = v.getId();
        if(idButton == R.id.buttonLogin){
            login();
        }
        if(idButton == R.id.buttonRegisterLogin){
            finish();
            Intent registerActivity = new Intent(this,RegisterActivity.class);
            startActivity(registerActivity);
        }
    }

    public void login(){
        final String email = editEmail.getText().toString();
        final String password = editPassword.getText().toString();

        if((email.equals("")) || (password.equals(""))){
            showMessage("Lengkapi data !");
        }else{
            String[] emailCheck = email.split("@");
            if(emailCheck.length < 2){
                showMessage("Email salah !");
            }else {
                CollectionReference users = db.collection("users");
                Query query = users.whereEqualTo("email", email).whereEqualTo("password",password);
                query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (DocumentSnapshot document : task.getResult()) {
                                checkAda = true;
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("sipresmawa", 0);
                                SharedPreferences.Editor editor = pref.edit();
                                editor.putString("no_id", document.getData().get("no_id").toString());
                                editor.apply();
                                finish();
                                Intent mainActivity = new Intent(LoginActivity.this,MainActivity.class);
                                startActivity(mainActivity);
                            }
                            if(!checkAda){
                                showMessage("Email atau Password salah  !");
                            }
                        }
                    }
                });
            }
        }
    }

    public void showMessage(String text){
        AlertDialog.Builder alert  = new AlertDialog.Builder(this);
        alert.setMessage(text);
        alert.setPositiveButton("OK", null);
        alert.create().show();
    }
}
