package com.renandatta.sipresmawa;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView imageButtonHome,imageButtonSearch,imageButtonHistory,imageButtonProfile,imageButtonLogout;
    private TextView textNoId,textName,textEmail,textPhone;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        imageButtonHome = findViewById(R.id.imageButttonHome);
        imageButtonSearch = findViewById(R.id.imageButtonSearch);
        imageButtonHistory = findViewById(R.id.imageButtonHistory);
        imageButtonProfile = findViewById(R.id.imageButtonProfile);
        imageButtonLogout = findViewById(R.id.imageButtonLogout);
        textNoId = findViewById(R.id.textNoId);
        textName = findViewById(R.id.textName);
        textEmail = findViewById(R.id.textEmail);
        textPhone = findViewById(R.id.textPhone);

        imageButtonHome.setOnClickListener(this);
        imageButtonSearch.setOnClickListener(this);
        imageButtonHistory.setOnClickListener(this);
        imageButtonProfile.setOnClickListener(this);
        imageButtonLogout.setOnClickListener(this);

        db = FirebaseFirestore.getInstance();

        getData();
    }

    @Override
    public void onClick(View v){
        int idButton = v.getId();
        if(idButton == R.id.imageButttonHome){
            finish();
            Intent mainActivity = new Intent(this,MainActivity.class);
            startActivity(mainActivity);
        }
        if(idButton == R.id.imageButtonSearch){
            finish();
            Intent searchActivity = new Intent(this,SearchActivity.class);
            startActivity(searchActivity);
        }
        if(idButton == R.id.imageButtonHistory){
            finish();
            Intent historyActivity = new Intent(this,HistoryActivity.class);
            startActivity(historyActivity);
        }
        if(idButton == R.id.imageButtonProfile){
            finish();
            Intent profileActivity = new Intent(this,ProfileActivity.class);
            startActivity(profileActivity);
        }
        if(idButton == R.id.imageButtonLogout){
            SharedPreferences pref = getApplicationContext().getSharedPreferences("sipresmawa", 0);
            SharedPreferences.Editor editor = pref.edit();
            editor.remove("no_id");
            editor.apply();

            finish();
            Intent loginActivity = new Intent(this,LoginActivity.class);
            startActivity(loginActivity);
        }
    }

    public void getData(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("sipresmawa", 0);
        final String no = pref.getString("no_id", null);
        CollectionReference users = db.collection("users");
        Query query = users.whereEqualTo("no_id", no);
        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (DocumentSnapshot document : task.getResult()) {
                        textNoId.setText(": "+document.getData().get("no_id").toString());
                        textName.setText(": "+document.getData().get("name").toString());
                        textEmail.setText(": "+document.getData().get("email").toString());
                        textPhone.setText(": "+document.getData().get("phone").toString());
                    }
                }
            }
        });
    }
}
