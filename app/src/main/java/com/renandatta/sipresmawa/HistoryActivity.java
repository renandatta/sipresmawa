package com.renandatta.sipresmawa;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class HistoryActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView imageButtonHome,imageButtonSearch,imageButtonHistory,imageButtonProfile,imageButtonLogout;
    private Button buttonAkanDatang,buttonSudahLewat;
    private ListView listViewEvent;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        imageButtonHome = findViewById(R.id.imageButttonHome);
        imageButtonSearch = findViewById(R.id.imageButtonSearch);
        imageButtonHistory = findViewById(R.id.imageButtonHistory);
        imageButtonProfile = findViewById(R.id.imageButtonProfile);
        imageButtonLogout = findViewById(R.id.imageButtonLogout);
        buttonAkanDatang = findViewById(R.id.buttonAkanDatang);
        buttonSudahLewat = findViewById(R.id.buttonSudahLewat);
        listViewEvent = findViewById(R.id.listViewEvent);
        listViewEvent.setDivider(null);

        imageButtonHome.setOnClickListener(this);
        imageButtonSearch.setOnClickListener(this);
        imageButtonHistory.setOnClickListener(this);
        imageButtonProfile.setOnClickListener(this);
        imageButtonLogout.setOnClickListener(this);
        buttonSudahLewat.setOnClickListener(this);
        buttonAkanDatang.setOnClickListener(this);

        buttonAkanDatang.setBackgroundColor(Color.BLUE);
        buttonSudahLewat.setBackgroundColor(Color.GRAY);

        db = FirebaseFirestore.getInstance();
        getData(false);
    }

    @Override
    public void onClick(View v){
        int idButton = v.getId();
        if(idButton == R.id.imageButttonHome){
            finish();
            Intent mainActivity = new Intent(this,MainActivity.class);
            startActivity(mainActivity);
        }
        if(idButton == R.id.imageButtonSearch){
            finish();
            Intent searchActivity = new Intent(this,SearchActivity.class);
            startActivity(searchActivity);
        }
        if(idButton == R.id.imageButtonHistory){
            finish();
            Intent historyActivity = new Intent(this,HistoryActivity.class);
            startActivity(historyActivity);
        }
        if(idButton == R.id.imageButtonProfile){
            finish();
            Intent profileActivity = new Intent(this,ProfileActivity.class);
            startActivity(profileActivity);
        }
        if(idButton == R.id.imageButtonLogout){
            SharedPreferences pref = getApplicationContext().getSharedPreferences("sipresmawa", 0);
            SharedPreferences.Editor editor = pref.edit();
            editor.remove("no_id");
            editor.apply();

            finish();
            Intent loginActivity = new Intent(this,LoginActivity.class);
            startActivity(loginActivity);
        }
        if(idButton == R.id.buttonAkanDatang){
            buttonAkanDatang.setBackgroundColor(Color.BLUE);
            buttonSudahLewat.setBackgroundColor(Color.GRAY);
            getData(false);
        }
        if(idButton == R.id.buttonSudahLewat){
            buttonAkanDatang.setBackgroundColor(Color.GRAY);
            buttonSudahLewat.setBackgroundColor(Color.BLUE);
            getData(true);
        }
    }

    public void getData(Boolean sebelum){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("sipresmawa", 0);
        final String no = pref.getString("no_id", null);
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        final String dateNow = df.format(c);

        CollectionReference events = db.collection("participants");
        Query query = events.whereEqualTo("no_id",no);
        if(sebelum){
            query = query.whereLessThan("date_event",dateNow);
        }else{
            query = query.whereGreaterThan("date_event",dateNow);
        }
        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (DocumentSnapshot document : task.getResult()) {
                        //String idEvent = document.getData().get("event_id").toString();
                        displayEvent(document.getData().get("event_id").toString());
                    }
                } else {
                    Log.w("TEST", "Error getting documents.", task.getException());
                }
            }
        });
    }

    public void displayEvent(String idEvent){
        final ArrayList<HashMap<String,String>> listEvent = new ArrayList<>();
        db.collection("events")
                .document(idEvent)
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>(){
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        HashMap<String,String> list = new HashMap<>();
                        list.put("id",document.getId());
                        list.put("event",document.getData().get("event").toString());
                        list.put("description",document.getData().get("description").toString());
                        list.put("date",document.getData().get("date").toString());
                        listEvent.add(list);

                        ListAdapter adapter = new SimpleAdapter(
                                HistoryActivity.this, listEvent, R.layout.list_event_2,
                                new String[]{"event","description","date"},
                                new int[]{R.id.textEventName,R.id.textEventDescription,R.id.textEventDate}
                        );
                        listViewEvent.setAdapter(adapter);
                    }
                } else {
                    Log.d("TEST", "get failed with ", task.getException());
                }
            }
        });
    }

}
