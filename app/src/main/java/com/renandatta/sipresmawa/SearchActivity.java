package com.renandatta.sipresmawa;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SearchActivity extends AppCompatActivity implements View.OnClickListener,ListView.OnItemClickListener {

    private ImageView imageButtonHome,imageButtonSearch,imageButtonHistory,imageButtonProfile,imageButtonLogout;
    private Button buttonSearch;
    private EditText editSearchJudul;
    private ListView listViewEvent;
    private FirebaseFirestore db;
    private Boolean checkSudahAda = false;
    private String dateEvent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        imageButtonHome = findViewById(R.id.imageButttonHome);
        imageButtonSearch = findViewById(R.id.imageButtonSearch);
        imageButtonHistory = findViewById(R.id.imageButtonHistory);
        imageButtonProfile = findViewById(R.id.imageButtonProfile);
        imageButtonLogout = findViewById(R.id.imageButtonLogout);
        buttonSearch = findViewById(R.id.buttonSearchJudul);
        editSearchJudul = findViewById(R.id.editSearchJudul);
        listViewEvent = findViewById(R.id.listViewEvent);
        listViewEvent.setDivider(null);

        imageButtonHome.setOnClickListener(this);
        imageButtonSearch.setOnClickListener(this);
        imageButtonHistory.setOnClickListener(this);
        imageButtonProfile.setOnClickListener(this);
        imageButtonLogout.setOnClickListener(this);
        buttonSearch.setOnClickListener(this);
        listViewEvent.setOnItemClickListener(this);

        db = FirebaseFirestore.getInstance();
    }

    @Override
    public void onClick(View v){
        int idButton = v.getId();
        if(idButton == R.id.imageButttonHome){
            finish();
            Intent mainActivity = new Intent(this,MainActivity.class);
            startActivity(mainActivity);
        }
        if(idButton == R.id.imageButtonSearch){
            finish();
            Intent searchActivity = new Intent(this,SearchActivity.class);
            startActivity(searchActivity);
        }
        if(idButton == R.id.imageButtonHistory){
            finish();
            Intent historyActivity = new Intent(this,HistoryActivity.class);
            startActivity(historyActivity);
        }
        if(idButton == R.id.imageButtonProfile){
            finish();
            Intent profileActivity = new Intent(this,ProfileActivity.class);
            startActivity(profileActivity);
        }
        if(idButton == R.id.imageButtonLogout){
            SharedPreferences pref = getApplicationContext().getSharedPreferences("sipresmawa", 0);
            SharedPreferences.Editor editor = pref.edit();
            editor.remove("no_id");
            editor.apply();

            finish();
            Intent loginActivity = new Intent(this,LoginActivity.class);
            startActivity(loginActivity);
        }
        if(idButton == R.id.buttonSearchJudul){
            getEvent();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        HashMap map = (HashMap)parent.getItemAtPosition(position);
        String idEvent = map.get("id").toString();
        konfirmasiEvent(idEvent);
    }

    public void getEvent(){
        final String judul = editSearchJudul.getText().toString();

        db.collection("events").whereGreaterThanOrEqualTo("event", judul)
            .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        ArrayList<HashMap<String,String>> listEvent = new ArrayList<>();
                        for (DocumentSnapshot document : task.getResult()) {
                            HashMap<String,String> list = new HashMap<>();
                            list.put("id",document.getId());
                            list.put("event",document.getData().get("event").toString());
                            list.put("description",document.getData().get("description").toString());
                            list.put("date",document.getData().get("date").toString());
                            listEvent.add(list);
                        }
                        ListAdapter adapter = new SimpleAdapter(
                                SearchActivity.this, listEvent, R.layout.list_event,
                                new String[]{"event","description","date"},
                                new int[]{R.id.textEventName,R.id.textEventDescription,R.id.textEventDate}
                        );
                        listViewEvent.setAdapter(adapter);
                    } else {
                        Log.w("TEST", "Error getting documents.", task.getException());
                    }
                }
            });
    }

    public void konfirmasiEvent(final String idEvent){
        db.collection("events")
                .document(idEvent)
                .get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>(){
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        dateEvent = document.getData().get("date").toString();

                        AlertDialog.Builder alert  = new AlertDialog.Builder(SearchActivity.this);
                        alert.setCancelable(true);
                        alert.setTitle("Konfirmasi");
                        alert.setMessage("Anda yakin ingin mengikuti event\n'"+document.getData().get("event")+"' ?");
                        alert.setPositiveButton("Confirm",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        pilihEvent(idEvent);
                                    }
                                });
                        alert.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alert.create().show();
                    }
                } else {
                    Log.d("TEST", "get failed with ", task.getException());
                }
            }
        });
    }

    public void pilihEvent(final String idEvent){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("sipresmawa", 0);
        final String no = pref.getString("no_id", null);

        db.collection("participants")
                .whereEqualTo("no_id", no).whereEqualTo("event_id",idEvent)
                .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (DocumentSnapshot document : task.getResult()) {
                        checkSudahAda = true;
                        showMessage("Anda sudah terdaftar event ini");
                    }
                    if(!checkSudahAda) {
                        Map<String, Object> participant = new HashMap<>();
                        participant.put("no_id", no);
                        participant.put("event_id", idEvent);
                        participant.put("date_event", dateEvent.replaceAll("-",""));
                        db.collection("participants")
                                .add(participant)
                                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                    @Override
                                    public void onSuccess(DocumentReference documentReference) {
                                        showMessage("Pendaftaran berhasil !");
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        showMessage("Pendaftaran gagal !");
                                    }
                                });
                    }
                }
            }
        });
    }

    public void showMessage(String text){
        AlertDialog.Builder alert  = new AlertDialog.Builder(this);
        alert.setMessage(text);
        alert.setPositiveButton("OK", null);
        alert.create().show();
    }
}
